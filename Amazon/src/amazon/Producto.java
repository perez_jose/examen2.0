/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package amazon;

/**
 *
 * @author USUARIO
 */
public class Producto {
    private String Nombre;
    private String Descriccion;
    private int Cantidad;
    private double Precio;
    private int iva;

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public String getDescriccion() {
        return Descriccion;
    }

    public void setDescriccion(String Descriccion) {
        this.Descriccion = Descriccion;
    }

    public int getCantidad() {
        return Cantidad;
    }

    public void setCantidad(int Cantidad) {
        this.Cantidad = Cantidad;
    }

    public double getPrecio() {
        return Precio;
    }

    public void setPrecio(double Precio) {
        this.Precio = Precio;
    }

    public int getIva() {
        return iva;
    }

    public void setIva() {
        this.iva = 12/100;
    }
    
    
}
