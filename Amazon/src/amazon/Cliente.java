/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package amazon;

/**
 *
 * @author USUARIO
 */
public class Cliente implements IImprimircliente{
    private String Nombre;
    private String Apellidos;
    private String NombreUsuario;
    private int Contrasenia;
    private String Correo;

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public String getApellidos() {
        return Apellidos;
    }

    public void setApellidos(String Apellidos) {
        this.Apellidos = Apellidos;
    }

    public String getNombreUsuario() {
        return NombreUsuario;
    }

    public void setNombreUsuario(String NombreUsuario) {
        this.NombreUsuario = NombreUsuario;
    }

    public int getContrasenia() {
        return Contrasenia;
    }

    public void setContrasenia(int Contrasenia) {
        this.Contrasenia = Contrasenia;
    }

    public String getCorreo() {
        return Correo;
    }

    public void setCorreo(String Correo) {
        this.Correo = Correo;
    }

    @Override
    public String Imprimirclientes() {
        return this.getNombre()+" "+this.getApellidos()+" "+this.getNombreUsuario()+" "+this.getCorreo()+" "+this.getContrasenia();
    }
    
    
}
