/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package polimorfismointerfaces;

/**
 *
 * @author USUARIO
 */
public class Dueño implements IImprimir{
    private String Nombre;
    private String Apellido;
    private String NumeroTelefonico;
    private String DireccionDomicilio;
    private String Cedula;

    public Dueño(String Nombre, String Apellido, String NumeroTelefonico, String DireccionDomicilio, String Cedula) {
        this.Nombre = Nombre;
        this.Apellido = Apellido;
        this.NumeroTelefonico = NumeroTelefonico;
        this.DireccionDomicilio = DireccionDomicilio;
        this.Cedula = Cedula;
    }
    

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public String getApellido() {
        return Apellido;
    }

    public void setApellido(String Apellido) {
        this.Apellido = Apellido;
    }

    public String getNumeroTelefonico() {
        return NumeroTelefonico;
    }

    public void setNumeroTelefonico(String NumeroTelefonico) {
        this.NumeroTelefonico = NumeroTelefonico;
    }

    public String getDireccionDomicilio() {
        return DireccionDomicilio;
    }

    public void setDireccionDomicilio(String DireccionDomicilio) {
        this.DireccionDomicilio = DireccionDomicilio;
    }

    public String getCedula() {
        return Cedula;
    }

    public void setCedula(String Cedula) {
        this.Cedula = Cedula;
    }

    @Override
    public void Imprimir() {
        System.out.println(this.getNombre()+" "+this.getApellido()+ " "+this.getCedula()+" "+this.getDireccionDomicilio()+" "+this.getNumeroTelefonico());
    }
    
    
}
