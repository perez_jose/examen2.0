/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package polimorfismointerfaces;

import java.sql.Time;
import java.util.Date;

/**
 *
 * @author USUARIO
 */
public class RegistroIngreso {
    private Dueño DueñoDeMascota;
    private Mascota Mascota;
    private Time HoraEntrada;
    private Date FechaEntrada;
    private String MotivoIngreso;

    public RegistroIngreso(Dueño DueñoDeMascota, Mascota Mascota, Time HoraEntrada, Date FechaEntrada, String MotivoIngreso) {
        this.DueñoDeMascota = DueñoDeMascota;
        this.Mascota = Mascota;
        this.HoraEntrada = HoraEntrada;
        this.FechaEntrada = FechaEntrada;
        this.MotivoIngreso = MotivoIngreso;
    }


    


    public Dueño getDueñoDeMascota() {
        return DueñoDeMascota;
    }

    public void setDueñoDeMascota(Dueño DueñoDeMascota) {
        this.DueñoDeMascota = DueñoDeMascota;
    }

    public Mascota getMascota() {
        return Mascota;
    }

    public void setMascota(Mascota Mascota) {
        this.Mascota = Mascota;
    }

    public Time getHoraEntrada() {
        return HoraEntrada;
    }

    public void setHoraEntrada(Time HoraEntrada) {
        this.HoraEntrada = HoraEntrada;
    }

    public Date getFechaEntrada() {
        return FechaEntrada;
    }

    public void setFechaEntrada(Date FechaEntrada) {
        this.FechaEntrada = FechaEntrada;
    }

    public String getMotivoIngreso() {
        return MotivoIngreso;
    }

    public void setMotivoIngreso(String MotivoIngreso) {
        this.MotivoIngreso = MotivoIngreso;
    }
    
   public void ImprimirDatos(){
       System.out.println(this.DueñoDeMascota.getApellido()+" "+this.DueñoDeMascota.getNombre());
       System.out.println(this.Mascota.getNombre());
       System.out.println(this.getFechaEntrada());
       System.out.println(this.getHoraEntrada());
       System.out.println(this.getMotivoIngreso());
   } 
}
