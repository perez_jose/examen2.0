/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package polimorfismointerfaces;

import java.util.Date;

/**
 *
 * @author USUARIO
 */
public class Mascota implements IImprimir{
    private String Nombre;
    private int Identificador;
    private String Raza;
    private String Apodo;
    private Date FechaDeNacimiento;
    
    
    
    public String Comer(){
    return this.Nombre+" "+"Esta comiendo";
        
    }
    public String Dormir (){
        return this.Nombre+" "+" Esta durmiendo";
        
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public String getApodo() {
        return Apodo;
    }

    public void setApodo(String Apodo) {
        this.Apodo = Apodo;
    }

    public Date getFechaDeNacimiento() {
        return FechaDeNacimiento;
    }

    public void setFechaDeNacimiento(Date FechaDeNacimiento) {
        this.FechaDeNacimiento = FechaDeNacimiento;
    }

    public int getIdentificador() {
        return Identificador;
    }

    public void setIdentificador(int Identificador) {
        this.Identificador = Identificador;
    }

    public String getRaza() {
        return Raza;
    }

    public void setRaza(String Raza) {
        this.Raza = Raza;
    }

    @Override
    public void Imprimir() {
        System.out.println(this.Nombre+" "+this.Raza);
    }

  
   
    
}
